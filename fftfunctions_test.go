package freeform

import (
	"fmt"
	"testing"
)

func TestFuncMap(t *testing.T) {
	var fn string
	const passphrase = "p1"
	fm := FuncMap(nil, passphrase)
	sc := makeSecurecookie(passphrase)
	sc2 := makeSecurecookie(passphrase + passphrase)

	if got, want := len(fm), 5; got != want {
		t.Errorf("want map len %v, got %v", want, got)
	}

	fn = "ffurl"
	if f, ok := fm[fn].(func(string) (string, error)); ok {
		want := "/api/a"
		got, err := f(want)
		if err != nil {
			t.Errorf("%q error: %v", fn, err)
		} else if got != want {
			t.Errorf("want %v, got %v", want, got)
		}
	} else {
		t.Errorf("%q missing or wrong type %T", fn, fm[fn])
	}

	fn = "ffreg"
	if f, ok := fm[fn].(func(string, ...string) string); ok {
		want := "$reg"
		got := f(want, "v1", "v2")
		if got != want {
			t.Errorf("want %v, got %v", want, got)
		}
	} else {
		t.Errorf("%q missing or wrong type %T", fn, fm[fn])
	}

	fn = "ffadd"
	if f, ok := fm[fn].(func(string, string) string); ok {
		want := "another-value"
		got := f("$reg", want)
		if got != want {
			t.Errorf("want %v, got %v", want, got)
		}
	} else {
		t.Errorf("%q missing or wrong type %T", fn, fm[fn])
	}

	fn = "fffreestamp"
	if _, ok := fm[fn].(func() (string, error)); !ok {
		t.Errorf("%q missing or wrong type %T", fn, fm[fn])
	}

	fn = "ffstamp"
	if f, ok := fm[fn].(func() (string, error)); ok {
		want := len(`MTU4NjExOTU5NnxhMndLQ1FnRDVhazkwQjZjcXlVdXYtWkNXNFhHN2lDbTIxOFJUOGpGdWtUM1c5NnFJbW5EWEtiT2Fjc1lyNFhOcTh0SU5LcUpxbk54UGtXU2J4VG5KOG9DeFZmTlppWXN4U1czaXRkanpXRnh4bkpWQTR3bGs3TVdYSUk9fEHx8o1CI_Mp6PKwjQAgcub2W7S42E6kRm6iO9FSENR4`)
		got, err := f()
		t.Log(got)
		if err != nil {
			t.Errorf("%q error: %v", fn, err)
		} else if len(got) != want {
			t.Errorf("want len %v, got %v", want, len(got))
		} else {
			var st state
			err := sc.Decode(securecookieName, got, &st)
			if err != nil {
				t.Errorf("can't decode: %v", err)
			}
			txt := fmt.Sprint(st)
			want := `map[:[/api/a] $reg:[v1 v2 another-value]]`
			if txt != want {
				t.Errorf("want %v, got %v", want, txt)
			}

			err = sc2.Decode(securecookieName, got, &st)
			if err != nil {
				if got, want := err.Error(), `securecookie: the value is not valid`; got != want {
					t.Errorf("want %v, got %v", want, got)
				}
			} else {
				t.Error("should be an error, but is nil")
			}
		}
	} else {
		t.Errorf("%q missing or wrong type %T", fn, fm[fn])
	}
}
