/*
Package freeform is a Go library to secure form posts for which no
form field name schema is known in advance, i.e. not until
it is generated by a template.

During template execution, a map containing allowed form
field names and allowed values is created on the fly via
some template functions. When all names and values are registered
a secured and hashed blob as string can be retrieved and sent to
the client e.g. in a hidden form field with a special name. This
string is called *stamp*.

On post, an http handler middleware component is provided that
checks the validity of this blob and then checks that only
previously registered form field names are present, and that
their values were registered as valid during construction.

Template Functions

`ffurl` "api-path" -> "api-path" : use this function in a form action to post to "api-path"

`ffreg` "name" ["value" ...] -> "name" : to register the "name" of a form field and optionally list allowed values.

`ffadd` "name" "value" -> "value" : add a "value" as allowed for field "name". Implies `ffreg` "name".

`fffreestamp` -> hashed and encrypted string : put this last into a hidden field in the form. The functions state is cleared by this function call.

`ffstamp` -> like fffreestamp, but errors, if ffurl was never called

Middleware Usage

To check a freeform for validity, wrap the `http.Handler` handling the `post` request
within `freeform.ValidateHandler`. */
package freeform
