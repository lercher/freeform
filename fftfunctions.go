package freeform

import (
	"crypto/sha256"
	"errors"
	"html/template"
	"net/url"

	"github.com/gorilla/securecookie"
)

// freeform template functions

const securecookieName = "ff"

// FuncMap adds new instances of the well-know freeform template
// functions
//   ffurl, ffreg, ffadd, ffstamp
// to a template.FuncMap. As they are stateful, templates must be
// copied prior to execution and provided with such a new FuncMap.
// The passphrase is used to generate proper keys for hashes and
// encription.
// In any case a new FuncMap is created, the given FuncMap is copied into the new map, not overwriting ff-functions.
func FuncMap(fm template.FuncMap, passphrase string) template.FuncMap {
	s := make(state)
	sc := makeSecurecookie(passphrase)

	fmap := template.FuncMap{
		"ffurl": func(raw string) (string, error) {
			u, err := url.Parse(raw)
			if err != nil {
				return raw, err
			}
			s[""] = []string{u.Path}
			return raw, nil
		},

		"ffreg": func(name string, values ...string) string {
			s[name] = values
			return name
		},

		"ffadd": func(name string, value string) string {
			s[name] = append(s[name], value)
			return value
		},

		"ffstamp": func() (string, error) {
			if _, ok := s[""]; !ok {
				return "", errors.New("ffstamp called without prior call to ffurl")
			}
			cv, err := sc.Encode(securecookieName, s)
			if err != nil {
				return "", err
			}
			s = make(state) // reset state for a new form
			return cv, nil
		},

		"fffreestamp": func() (string, error) {
			cv, err := sc.Encode(securecookieName, s)
			if err != nil {
				return "", err
			}
			s = make(state) // reset state for a new form
			return cv, nil
		},
	}

	for k, v := range fm {
		if _, ok := fmap[k]; !ok {
			fmap[k] = v
		}
	}

	return fmap
}

// state maps a form field name to its allowed values.
// An empty slice allows arbitrary values.
// The empty key is reserved for the post url
type state map[string][]string

func makeSecurecookie(passphrase string) *securecookie.SecureCookie {
	hashKey := sha256.Sum256([]byte("hash:" + passphrase))
	blockKey := sha256.Sum256([]byte("block:" + passphrase))
	// Hash keys should be at least 32 bytes long
	// Block keys should be 16 bytes (AES-128) or 32 bytes (AES-256) long.
	return securecookie.New(hashKey[:], blockKey[:]).MaxLength(0)
}
