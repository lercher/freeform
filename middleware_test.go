package freeform

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
)

// p1 encoded for map[:[/api/a] $reg:[v1 v2 another-value]] with an outdated timestamp
const stampOKforP1Sample = `MTU4NjExNzc3OHxzYXdYQklXZ2N0TEVLcjc1ZmhJOUhmdFotWkJZY1VRbjAxQnN3VklPSVlkMU1QMUZ3UlBORnlaTHNuRVN3RmMzbHRoV3llUkJtRndNS19nVTZwRC1WSzl2dDFtVWFyOTA5R3dHSkp6QjA5THZXRkRfdXhzMmROYkFCaHc9fJSZY6Csi0exQLWiMa53INW2PubMq8QaCwsCj7BJcuYC`

func stampFor(t *testing.T, url string, reg []string) string {
	fm := FuncMap(nil, "p1")
	ffurl := fm["ffurl"].(func(string) (string, error))
	ffreg := fm["ffreg"].(func(string, ...string) string)
	ffadd := fm["ffadd"].(func(string, string) string)
	ffstamp := fm["ffstamp"].(func() (string, error))
	fffreestamp := fm["fffreestamp"].(func() (string, error))
	if url != "*" {
		_, _ = ffurl(url)
	}
	_ = ffreg("$reg", reg...)
	_ = ffadd // add not used
	stamp, err := ffstamp()
	if url != "*" {
		if err != nil {
			t.Error(err)
		}
	} else {
		if err == nil {
			t.Fatalf("want error 'ffstamp called without prior call to ffurl', got %v", err)
		} else if err.Error() != `ffstamp called without prior call to ffurl` {
			t.Fatalf("want error 'ffstamp called without prior call to ffurl', got %v", err)
		}
		stamp, _ = fffreestamp()
	}
	return stamp
}

func TestValidateHandler(t *testing.T) {
	handler := func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "OK")
	}

	v := validater{
		sc:         makeSecurecookie("p1"),
		inner:      http.HandlerFunc(handler),
		stamp:      `__ff`,
		csrf:       `__csrf`,
		errHandler: httpErrorHandler,
	}

	// p1 encoded for map[:[/api/a] $reg:[v1 v2 another-value]] with a current timestamp
	stampValue := stampFor(t, "/api/a", []string{"v1", "v2", "another-value"})
	t.Log(stampValue)

	stampValueForNullURL := stampFor(t, "", []string{"v1", "v2", "another-value"})
	t.Log(stampValueForNullURL)

	stampValueForNoURLAtAll := stampFor(t, "*", []string{"v1", "v2", "another-value"})
	t.Log(stampValueForNoURLAtAll)

	var req *http.Request
	var form io.Reader

	// nil form
	form = nil
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusBadRequest, `freeform no stamp field "__ff"`)

	// no __ff field
	form = makeForm(t, v.csrf, "anything goes")
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusBadRequest, `freeform no stamp field "__ff"`)

	// correct stamp field and correct path
	form = makeForm(t, v.stamp, stampValue)
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusOK, `OK`)

	// correct stamp field but wrong path
	form = makeForm(t, v.stamp, stampValue)
	req = httptest.NewRequest("POST", "http://example.com/wrong/url/path", form)
	check(t, v, req, http.StatusBadRequest, `freeform registered path "/api/a" doesn't match actual path "/wrong/url/path"`)

	// correct stamp field and correct path and csrf field present
	form = makeForm(t, v.stamp, stampValue, v.csrf, "anything goes")
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusOK, `OK`)

	// correct stamp field and correct path and csrf field present and $reg field with value
	form = makeForm(t, v.stamp, stampValue, v.csrf, "anything goes", "$reg", "v1")
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusOK, `OK`)

	// correct stamp field and correct path and csrf field present and $reg field with 2 values
	form = makeForm(t, v.stamp, stampValue, v.csrf, "anything goes", "$reg", "v1", "$reg", "v2")
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusOK, `OK`)

	// correct stamp field and correct path and csrf field present and $reg field with 1st wrong value
	form = makeForm(t, v.stamp, stampValue, v.csrf, "anything goes", "$reg", "(v1)", "$reg", "v2")
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusBadRequest, `freeform field "$reg" has invalid value "(v1)"`)

	// correct stamp field and correct path and csrf field present and $reg field with 2nd wrong value
	form = makeForm(t, v.stamp, stampValue, v.csrf, "anything goes", "$reg", "v1", "$reg", "(v2)")
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusBadRequest, `freeform field "$reg" has invalid value "(v2)"`)

	// correct stamp field and correct path and csrf field present and $reg field 2 values and one extra field
	form = makeForm(t, v.stamp, stampValue, v.csrf, "anything goes", "$reg", "v1", "$reg", "v2", "$extra", "extra-value")
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusBadRequest, `unregistered freeform field "$extra"`)

	// ----------------------------------------------------------------------------------------- We check against null URL ---------------

	// correct stamp field and correct path and csrf field present and $reg field with 2nd wrong value
	form = makeForm(t, v.stamp, stampValueForNullURL, v.csrf, "anything goes", "$reg", "v1", "$reg", "(v2)")
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusBadRequest, `freeform registered path "" doesn't match actual path "/api/a"`)

	// correct stamp field and correct path and csrf field present and $reg field 2 values and one extra field
	form = makeForm(t, v.stamp, stampValueForNullURL, v.csrf, "anything goes", "$reg", "v1", "$reg", "v2", "$extra", "extra-value")
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusBadRequest, `freeform registered path "" doesn't match actual path "/api/a"`)

	// correct stamp field and correct path and csrf field present and $reg field with 2nd wrong value
	form = makeForm(t, v.stamp, stampValueForNullURL, v.csrf, "anything goes", "$reg", "v1", "$reg", "(v2)")
	req = httptest.NewRequest("POST", "http://example.com/wrong/url/path", form)
	check(t, v, req, http.StatusBadRequest, `freeform registered path "" doesn't match actual path "/wrong/url/path"`)

	// correct stamp field and correct path and csrf field present and $reg field 2 values and one extra field
	form = makeForm(t, v.stamp, stampValueForNullURL, v.csrf, "anything goes", "$reg", "v1", "$reg", "v2", "$extra", "extra-value")
	req = httptest.NewRequest("POST", "http://example.com/wrong/url/path", form)
	check(t, v, req, http.StatusBadRequest, `freeform registered path "" doesn't match actual path "/wrong/url/path"`)

	// ----------------------------------------------------------------------------------------- We check against no URL at all, any POST URL allowed ---------------

	// correct stamp field and correct path and csrf field present and $reg field with 2nd wrong value
	form = makeForm(t, v.stamp, stampValueForNoURLAtAll, v.csrf, "anything goes", "$reg", "v1", "$reg", "(v2)")
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusBadRequest, `freeform field "$reg" has invalid value "(v2)"`)

	// correct stamp field and correct path and csrf field present and $reg field 2 values and one extra field
	form = makeForm(t, v.stamp, stampValueForNoURLAtAll, v.csrf, "anything goes", "$reg", "v1", "$reg", "v2", "$extra", "extra-value")
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusBadRequest, `unregistered freeform field "$extra"`)

	// correct stamp field and correct path and csrf field present and $reg field with 2nd wrong value
	form = makeForm(t, v.stamp, stampValueForNoURLAtAll, v.csrf, "anything goes", "$reg", "v1", "$reg", "(v2)")
	req = httptest.NewRequest("POST", "http://example.com/wrong/url/path", form)
	check(t, v, req, http.StatusBadRequest, `freeform field "$reg" has invalid value "(v2)"`)

	// correct stamp field and correct path and csrf field present and $reg field 2 values and one extra field
	form = makeForm(t, v.stamp, stampValueForNoURLAtAll, v.csrf, "anything goes", "$reg", "v1", "$reg", "v2", "$extra", "extra-value")
	req = httptest.NewRequest("POST", "http://example.com/wrong/url/path", form)
	check(t, v, req, http.StatusBadRequest, `unregistered freeform field "$extra"`)

	// ----------------------------------------------------------------------------------------- Now we change the secret ---------------
	v.sc = makeSecurecookie("you shall not pass")

	// correct stamp field and correct path and csrf field present and $reg field with 2 values but decoded with wrong passphrase
	form = makeForm(t, v.stamp, stampValue, v.csrf, "anything goes", "$reg", "v1", "$reg", "v2")
	req = httptest.NewRequest("POST", "http://example.com/api/a", form)
	check(t, v, req, http.StatusBadRequest, `freeform decode: securecookie: the value is not valid`)

}

func check(t *testing.T, v validater, req *http.Request, wantStatus int, wantBody string) {
	t.Helper()

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	w := httptest.NewRecorder()
	v.ServeHTTP(w, req)
	resp := w.Result()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Error(err)
		return
	}
	b = bytes.TrimSpace(b)

	if got, want := resp.StatusCode, wantStatus; got != want {
		t.Errorf("want http status %v, got %v", want, got)
	}

	if got, want := string(b), wantBody; got != want {
		t.Errorf("want body %v, got %v", want, got)
	}
}

func makeForm(t *testing.T, namevalue ...string) io.Reader {
	t.Helper()
	data := url.Values{}
	for i := 0; i+1 < len(namevalue); i += 2 {
		k, v := namevalue[i], namevalue[i+1]
		// t.Logf("form set %v=%v", k, v)
		data.Add(k, v)
	}
	return strings.NewReader(data.Encode())
}
