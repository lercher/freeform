package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	"gitlab.com/lercher/freeform"
)

func main() {
	log.Println("This is the freeform testserver")

	t, err := template.New("").Funcs(freeform.FuncMap(nil, "")).ParseGlob("*.html")
	if err != nil {
		log.Fatalln(err)
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		tt, err := t.Clone()
		if err != nil {
			log.Fatalln(err)
		}
		tt = tt.Funcs(freeform.FuncMap(nil, "passphrase"))
		err = tt.ExecuteTemplate(w, "testform.html", nil)
		if err != nil {
			log.Fatalln(err)
		}
	})

	http.Handle("/submit", freeform.ValidateHandler("passphrase", `__ff`, ``)(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "OK")
	})))

	on := ":8080"
	log.Printf("Listening on http://localhost%s/", on)
	log.Fatalln(http.ListenAndServe(on, nil))
}
