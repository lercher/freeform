package freeform

import (
	"fmt"
	"net/http"

	"github.com/gorilla/securecookie"
)

type validater struct {
	inner      http.Handler
	sc         *securecookie.SecureCookie
	stamp      string
	csrf       string
	errHandler func(w http.ResponseWriter, r *http.Request, msg string, code int)
}

// ValidateHandler is an http middleware to validate free forms posts against its stamp value.
// Idempotent (safe) methods as defined by RFC7231 section 4.2.2.
// are not validated i.e. "GET", "HEAD", "OPTIONS" and "TRACE".
func ValidateHandler(passphrase, stampfieldname, csrffieldname string) func(h http.Handler) http.Handler {
	return CustomValidateHandler(passphrase, stampfieldname, csrffieldname, httpErrorHandler)
}

// CustomValidateHandler is an http middleware to validate free forms posts against its stamp value. See ValidateHandler for details on that.
// errHandler is called on detecting a validation error and the inner http.Handler is not called and no response is sent to the inner http.ResponseWriter.
// The function panics if errHandler is nil. If you want to inject only http.Error behavior as errHandler, consider using ValidateHandler.
// Normally errHandler is used to inject logic for custom logging of errors, and custom error responses other than the provided ones.
func CustomValidateHandler(passphrase, stampfieldname, csrffieldname string, errHandler func(w http.ResponseWriter, r *http.Request, msg string, code int)) func(h http.Handler) http.Handler {
	if errHandler == nil {
		panic("errHandler must not be <nil>")
	}
	sc := makeSecurecookie(passphrase)
	return func(inner http.Handler) http.Handler {
		v := &validater{inner: inner, sc: sc, stamp: stampfieldname, csrf: csrffieldname, errHandler: errHandler}
		return v
	}
}

func httpErrorHandler(w http.ResponseWriter, r *http.Request, msg string, code int) {
	http.Error(w, msg, code)
}

func (v validater) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet, http.MethodHead, http.MethodOptions, http.MethodTrace:
		// Idempotent (safe) methods as defined by RFC7231 section 4.2.2.
		// Don't validate, hand over to the inner handler
	default:
		// every non-safe verb is checked
		err := r.ParseForm()
		if err != nil {
			v.errHandler(w, r, fmt.Sprintf("freeform parse: %v", err), http.StatusBadRequest)
			return
		}

		cv := r.FormValue(v.stamp)
		if cv == "" {
			v.errHandler(w, r, fmt.Sprintf("freeform no stamp field %q", v.stamp), http.StatusBadRequest)
			return
		}

		var m state
		err = v.sc.Decode(securecookieName, cv, &m)
		if err != nil {
			v.errHandler(w, r, fmt.Sprintf("freeform decode: %v", err), http.StatusBadRequest)
			return
		}

		// check URL Path as stored in m[""]
		if stampedpaths, ok := m[""]; ok && len(stampedpaths) == 1 {
			stampedpath, actualpath := stampedpaths[0], r.URL.Path
			if stampedpath != actualpath {
				v.errHandler(w, r, fmt.Sprintf("freeform registered path %q doesn't match actual path %q", stampedpath, actualpath), http.StatusBadRequest)
				return
			}
		}

		// check all form fields
	withNextFormField:
		for k, vals := range r.Form { // r.Form is Values is map[string][]string
			if k == v.stamp || k == v.csrf {
				// the stamp field and the csrf field are always valid
				continue withNextFormField
			}

			if valids, ok := m[k]; ok {
				if len(valids) == 0 {
					continue withNextFormField
				}
			withNextSubmittedValue:
				for _, someSubmittedValue := range vals {
					for _, good := range valids {
						if good == someSubmittedValue {
							continue withNextSubmittedValue
						}
					}
					// someSubmittedValue is not good
					v.errHandler(w, r, fmt.Sprintf("freeform field %q has invalid value %q", k, someSubmittedValue), http.StatusBadRequest)
					return
				}
			} else {
				v.errHandler(w, r, fmt.Sprintf("unregistered freeform field %q", k), http.StatusBadRequest)
				return
			}
		}
	}

	// every field and value is ok, so hand the request over to the next handler:
	if v.inner != nil {
		v.inner.ServeHTTP(w, r)
	}
}
